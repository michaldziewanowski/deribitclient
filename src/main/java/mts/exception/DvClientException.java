package mts.exception;

public class DvClientException extends RuntimeException {
    private static final long serialVersionUID = 4247134376260271762L;
    private String dvErrorCode;
    private int dvResponseStatusCode;

    public DvClientException(String message, String dvErrorCode, int dvResponseStatusCode, Throwable cause) {
        super(message, cause);
        this.dvErrorCode = dvErrorCode;
        this.dvResponseStatusCode = dvResponseStatusCode;
    }

    public String getDvErrorCode() {
        return this.dvErrorCode;
    }

    public int getDvResponseStatusCode() {
        return this.dvResponseStatusCode;
    }
}
