package mts.model;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;

import java.io.IOException;

@Data
public class Response {

    private Long usOut;
    private Long usIn;
    private Long usDiff;
    private Boolean testnet;
    private Boolean success;
    private String message;
    private Object result;

    public <T> T returnResultAs(Class<T> clazz) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        try {
            String res = mapper.writeValueAsString(this.result);
            return new ObjectMapper().readValue(res, clazz);
        } catch (IOException e) {
            throw new RuntimeException("Couldn't map result to " + clazz.getSimpleName() + " class");
        }
    }

}
