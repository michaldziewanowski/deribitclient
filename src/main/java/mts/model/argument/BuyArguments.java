package mts.model.argument;

import lombok.Builder;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Builder
public class BuyArguments {

    private String instrument;
    private String type;
    private String label;
    private String timeInForce;
    private String postOnly;
    private String stopPx;
    private String execInst;
    private String adv;
    private Integer quantity;
    private Integer maxShow;
    private Float price;

    public MultiValueMap<String, String> asParams() {
        LinkedMultiValueMap<String, String> preParams = new LinkedMultiValueMap<>();

        if (instrument != null) {
            preParams.put("instrument", Collections.singletonList(instrument));
        }
        if (type != null) {
            preParams.put("type", Collections.singletonList(type));
        }
        if (label != null) {
            preParams.put("label", Collections.singletonList(label));
        }
        if (timeInForce != null) {
            preParams.put("timeInForce", Collections.singletonList(timeInForce));
        }
        if (postOnly != null) {
            preParams.put("postOnly", Collections.singletonList(postOnly));
        }
        if (stopPx != null) {
            preParams.put("stopPx", Collections.singletonList(stopPx));
        }
        if (execInst != null) {
            preParams.put("execInst", Collections.singletonList(execInst));
        }
        if (adv != null) {
            preParams.put("adv", Collections.singletonList(adv));
        }
        if (quantity != null) {
            preParams.put("quantity", Collections.singletonList(quantity+""));
        }
        if (maxShow != null) {
            preParams.put("maxShow", Collections.singletonList(maxShow+""));
        }
        if (price != null) {
            preParams.put("price", Collections.singletonList(price+""));
        }

        List<String> keys = new ArrayList<>(preParams.keySet());
        Collections.sort(keys);
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        keys.forEach(key -> params.put(key, preParams.get(key)));

        return params;
    }

    public MultiValueMap<String, String> emptyParams() {
        return new LinkedMultiValueMap<>();
    }
}
