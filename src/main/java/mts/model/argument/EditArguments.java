package mts.model.argument;


import lombok.Data;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Data
public class EditArguments {

    private String instrument;
    private String direction;
    private String type;
    private Float price;
    private Integer quantity;
    private String label;
    private String filledQuantity;
    private String avgPrice;
    private String commission;
    private String created;
    private String lastUpdate;
    private String state;
    private Boolean postOnly;
    private String adv;
    private String usd;
    private String post;

    public MultiValueMap<String,String> asParams () {
        LinkedMultiValueMap<String,String> preParams = new LinkedMultiValueMap<>();

        if (instrument != null) {
            preParams.put("instrument", Collections.singletonList(instrument));
        }
        if (type != null) {
            preParams.put("type", Collections.singletonList(type));
        }

        if (direction != null) {
            preParams.put("direction", Collections.singletonList(direction));
        }

        if (price != null) {
            preParams.put("price", Collections.singletonList(price + ""));
        }

        if (quantity != null) {
            preParams.put("quantity", Collections.singletonList(quantity + ""));
        }

        if (label != null) {
            preParams.put("label", Collections.singletonList(label));
        }

        if (filledQuantity != null) {
            preParams.put("filledQuantity", Collections.singletonList(filledQuantity));
        }

        if (avgPrice != null) {
            preParams.put("avgPrice", Collections.singletonList(avgPrice));
        }

        if (commission != null) {
            preParams.put("commission", Collections.singletonList(commission));
        }

        if (lastUpdate != null) {
            preParams.put("lastUpdate", Collections.singletonList(lastUpdate));
        }

        if (created != null) {
            preParams.put("created", Collections.singletonList(created));
        }

        if (state != null) {
            preParams.put("state", Collections.singletonList(state));
        }

        if (postOnly != null) {
            preParams.put("postOnly", Collections.singletonList(postOnly + ""));
        }

        if (adv != null) {
            preParams.put("adv", Collections.singletonList(adv));
        }

        if (usd != null) {
            preParams.put("usd", Collections.singletonList(usd));
        }

        if (post != null) {
            preParams.put("post", Collections.singletonList(post));
        }

        List<String> keys = new ArrayList<>(preParams.keySet());
        Collections.sort(keys);
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        keys.forEach(key -> params.put(key, preParams.get(key)));

        return params;

    }

    public MultiValueMap<String, String> emptyParams() {
        return new LinkedMultiValueMap<>();
    }

}
