package mts.model.result;

import lombok.Data;
import mts.model.Order;
import mts.model.Trade;

import java.util.List;

@Data
public class BuyResult {

    private Order order;
    private List<Trade> trades;
}
