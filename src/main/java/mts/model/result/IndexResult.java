package mts.model.result;

import lombok.Data;

@Data
public class IndexResult {

    private float btc;
    private float edp;
}
