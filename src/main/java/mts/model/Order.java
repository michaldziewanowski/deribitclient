package mts.model;

import lombok.Data;

@Data
public class Order {

    private String type;
    private String instrument;
    private String direction;
    private String label;
    private String state;
    private String orderId;
    private String execInst;
    private Long quantity;
    private Long amount;
    private Long lastUpdate;
    private Long created;
    private Long filledQuantity;
    private Long filledAmount;
    private Long max_show;
    private Long maxShowAmount;
    private Float commission;
    private Float avgPrice;
    private Float price;
    private Float implv;
    private Float usd;
    private Float stopPx;
    private Boolean postOnly;
    private Boolean api;
    private Boolean triggered;
    private Boolean adv;
}
