package mts.model;

import lombok.Data;

@Data

public class Trade {

    private String label;
    private String message;
    private Integer quantity;
    private Integer tradeSeq;
    private Integer amount;
    private Float price;
    private Boolean selfTrade;
    private Long matchingId;
}
