package mts.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import mts.enums.Endpoints;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Base64;

@Data
@AllArgsConstructor
public class Player {

    private String name;
    private String acKey;
    private String scKey;

    public String signature(MultiValueMap<String, String> params, Endpoints action) {
        long timestamp = System.currentTimeMillis();

        String signatureBuilder = "_=" + timestamp
                + "&_ackey=" + this.acKey
                + "&_acsec=" + this.scKey
                + "&_action=" + action.getAction();

        String signature = UriComponentsBuilder.fromUriString(signatureBuilder)
                .queryParams(params)
                .build()
                .toString()
                .replace("?", "&");

        byte[] hashed = DigestUtils.sha256(signature);
        return this.acKey + "." + timestamp + "." + Base64.getEncoder().encodeToString(hashed);
    }
}
