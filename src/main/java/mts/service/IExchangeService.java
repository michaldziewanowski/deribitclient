package mts.service;

import mts.model.Response;
import org.springframework.util.MultiValueMap;
import reactor.core.publisher.Mono;

public interface IExchangeService {

    Mono<Response> buy(MultiValueMap<String, String> params, String signature);

    Mono<Response> index();

    Mono<Response> sell(MultiValueMap<String, String> params, String signature);
}
