package mts.service;

import mts.model.Response;
import org.springframework.util.MultiValueMap;
import reactor.core.publisher.Mono;

public interface IApiConnector {

    Mono<Response> post(String signature, String url, MultiValueMap<String, String> params);

    Mono<Response> getPublic(String url, MultiValueMap<String, String> params);

    Mono<Response> getPrivate(String signature, String url, MultiValueMap<String, String> params);
}
