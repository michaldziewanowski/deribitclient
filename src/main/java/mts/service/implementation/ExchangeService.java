package mts.service.implementation;

import lombok.Data;
import mts.config.Context;
import mts.enums.Address;
import mts.enums.Endpoints;
import mts.model.Response;
import mts.service.IApiConnector;
import mts.service.IExchangeService;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import reactor.core.publisher.Mono;

@Data
public class ExchangeService implements IExchangeService {

    private IApiConnector apiConnector;
    private Address exchange;

    public ExchangeService(Address exchange) {
        this.apiConnector = new ApiConnector();
        this.exchange = exchange;
    }

    @Override
    public Mono<Response> buy(MultiValueMap<String, String> params, String signature) {
        String URL = Context.buildUrl(exchange, Endpoints.BUY);
        return apiConnector.post(signature, URL, params);
    }

    @Override
    public Mono<Response> index() {
        String URL = Context.buildUrl(exchange, Endpoints.INDEX);
        return apiConnector.getPublic(URL, new LinkedMultiValueMap<>());
    }

    @Override
    public Mono<Response> sell(MultiValueMap<String ,String> params, String signature) {
        String URL = Context.buildUrl(exchange, Endpoints.SELL);
        return apiConnector.post(signature, URL, params);
    }
}
