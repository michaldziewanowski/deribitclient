package mts.service.implementation;

import mts.config.Context;
import mts.model.Response;
import mts.service.IApiConnector;
import org.springframework.http.MediaType;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;

public class ApiConnector implements IApiConnector {

    private final String SIG_HEADER = "x-deribit-sig";

    private WebClient webClient;

    public ApiConnector() {
        this.webClient = WebClient.builder().build();
    }

    @Override
    public Mono<Response> post(String signature, String url, MultiValueMap<String, String> params) {
        return webClient
                .post()
                .uri(UriComponentsBuilder
                        .fromUriString(url)
                        .build()
                        .toString())
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .header(SIG_HEADER, signature)
                .body(BodyInserters.fromFormData(params))
                .retrieve()
                .bodyToMono(Response.class)
                .onErrorMap(Context::mapError);
    }

    @Override
    public Mono<Response> getPublic(String url, MultiValueMap<String, String> params) {
        return webClient
                .get()
                .uri(UriComponentsBuilder
                        .fromUriString(url)
                        .queryParams(params)
                        .build()
                        .toString())
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .retrieve()
                .bodyToMono(Response.class)
                .onErrorMap(Context::mapError);
    }

    @Override
    public Mono<Response> getPrivate(String signature, String url, MultiValueMap<String, String> params) {
        return webClient
                .get()
                .uri(UriComponentsBuilder
                        .fromUriString(url)
                        .queryParams(params)
                        .build()
                        .toString())
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .header(SIG_HEADER, signature)
                .retrieve()
                .bodyToMono(Response.class)
                .onErrorMap(Context::mapError);
    }
}
