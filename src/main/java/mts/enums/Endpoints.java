package mts.enums;

import org.springframework.http.HttpMethod;

import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;


public enum Endpoints {

    INDEX("/api/v1/public/index", "/public/index", GET),
    TIME("/api/v1/public/time", "/public/time", GET),
    TEST("/api/v1/public/test", "/public/time", GET),
    PING("/api/v1/public/ping", "/public/ping", GET),
    GET_INSTRUMENTS("/api/v1/public/getinstruments", "/public/getinstruments", GET),
    GET_CURRENCIES("/api/v1/public/getcurrencies", "/public/getcurrencies", GET),
    GET_ORDER_BOOK("/api/v1/public/getorderbook", "/public/getorderbook", GET),
    GET_LAST_TRADES("/api/v1/public/getlasttrades", "/public/getlasttrades", GET),
    GET_SUMMARY("/api/v1/public/getsummary", "/public/getsummary", GET),
    STATS("/api/v1/public/stats", "/public/stats", GET),
    GET_ANNOUNCEMENTS("/api/v1/public/getannouncements", "/public/getannouncements", GET),
    GET_LAST_SETTLEMENTS("/api/v1/public/getlastsettlements", "/public/getlastsettlements", GET),
    ACCOUNT("/api/v1/private/account", "/private/account", GET),
    SELL("/api/v1/private/sell", "/private/sell", POST),
    EDIT("/api/v1/private/edit", "/private/edit", POST),
    CANCEL("/api/v1/private/cancel", "/private/cancel", POST),
    CANCEL_ALL("/api/v1/private/cancelall", "/private/cancelall", POST),
    GET_OPEN_ORDERS("/api/v1/private/getopenorders", "/private/getopenorders", GET),
    POSITIONS("/api/v1/private/positions", "/private/positions", GET),
    ORDER_HISTORY("/api/v1/private/orderhistory", "/private/orderhistory", GET),
    ORDER_STATE("/api/v1/private/orderstate", "/private/orderstate", GET),
    TRADE_HISTORY("/api/v1/private/tradehistory", "/private/tradehistory", GET),
    NEW_ANNOUNCEMENTS("/api/v1/private/newannouncements", "/private/newannouncements", GET),
    GET_EMAIL_LANG("/api/v1/private/getemaillang", "/private/getemaillang", GET),
    SET_EMAIL_LANG("/api/v1/private/setemaillang", "/private/setemaillang", GET),
    SET_ANNOUNCEMENT_AS_READ("/api/v1/private/setannouncementasread", "/private/setannouncementasread", GET),
    SETTLEMENT_HISTORY("/api/v1/private/settlementhistory", "/private/settlementhistory", GET),
    BUY("/api/v1/private/buy", "/private/buy", POST);

    private String action;
    private String address;
    private HttpMethod method;

    Endpoints(String action, String address, HttpMethod method) {
        this.action = action;
        this.address = address;
        this.method = method;
    }

    public String getAction() {
        return this.action;
    }

    public String getAddress() {
        return address;
    }

    public HttpMethod getMethod() {
        return method;
    }
}
