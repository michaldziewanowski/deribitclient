package mts.enums;

public enum Address {

    MAIN("https://www.deribit.com/api/v1"),
    TEST("https://test.deribit.com/api/v1");

    private String url;

    Address(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
