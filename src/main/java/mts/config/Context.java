package mts.config;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import mts.exception.DvClientException;
import mts.controller.Controller;
import mts.enums.Address;
import mts.enums.Endpoints;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class Context {

    public static String apiKey = "CQ4cc1HmzVrW";
    public static String apiSecret = "QFZ7GNF6Y4FRER237BLOUGAHHGQ4A45N";

    public static void openWindow(String fxml, String title, double width, double height) {
        Parent root;
        try {
            root = FXMLLoader.load(Controller.class.getResource("/fxml/"+ fxml +".fxml"));
            Stage stage = new Stage();
            stage.setTitle(title);
            stage.setScene(new Scene(root, width, height));
            stage.setResizable(false);
            stage.show();
        } catch (IOException e) {
            throw new RuntimeException("Nie udalo sie zaladowac okna fxml o nazwie [" + fxml + "]\n" +
                    "Upewnij sie, ze wszystkie nody z kontrolera sa w fxmlu!", e);
        }
    }

    public static Throwable mapError(Throwable e) {
        if (e instanceof WebClientResponseException) {
            WebClientResponseException webClientResponseException = (WebClientResponseException)e;
            Map<String, String> dvErrorDetails = parseDvResponse(webClientResponseException.getResponseBodyAsString());
            return new DvClientException(dvErrorDetails.get("dv.error.message"), dvErrorDetails.get("dv.error.code"), webClientResponseException.getRawStatusCode(), e);
        } else {
            return new DvClientException("An error occurred while contacting DV site: " + e.getMessage(), null, 0, e);
        }
    }

    private static Map<String, String> parseDvResponse(String response) {
        HashMap<String, String> errorDetails = new HashMap<>();

        try {
            ObjectMapper objectMapper = new ObjectMapper();
            JsonNode json = objectMapper.readTree(response);
            JsonNode exceptionNode = json.get("exception");
            errorDetails.put("dv.error.code", Optional.ofNullable(exceptionNode)
                    .flatMap((node) -> Optional.ofNullable(node.get("errorCode")))
                    .map(JsonNode::asText).orElse("UNKNOWN_DV_ERROR"));
            errorDetails.put("dv.error.message", Optional.ofNullable(exceptionNode)
                    .flatMap((node) -> Optional.ofNullable(node.get("errorMessage")))
                    .map(JsonNode::asText).orElse("Unknown DV error"));
        } catch (IOException var5) {
            errorDetails.put("dv.error.message", "Unknown DV error");
            errorDetails.put("dv.error.code", "UNKNOWN_DV_ERROR");
        }
        return errorDetails;
    }

    public static String buildUrl(Address url, Endpoints endpoint) {
        return url.getUrl() + endpoint.getAddress();
    }
}
