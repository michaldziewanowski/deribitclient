package mts.controller;

import com.sun.org.apache.xpath.internal.operations.Or;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import mts.model.Order;
import mts.model.Player;
import mts.model.argument.BuyArguments;
import mts.model.argument.SellArguments;
import mts.model.result.BuyResult;
import mts.model.result.SellResult;
import mts.service.IExchangeService;
import mts.service.implementation.ExchangeService;
import org.springframework.util.MultiValueMap;
import reactor.core.publisher.Mono;

import static mts.config.Context.apiKey;
import static mts.config.Context.apiSecret;
import static mts.enums.Address.TEST;
import static mts.enums.Endpoints.BUY;
import static mts.enums.Endpoints.SELL;

public class Controller {

    @FXML TableView<Order> table;
    @FXML TableColumn<Order, String> type;
    @FXML TableColumn<Order, String> instrument;
    @FXML TableColumn<Order, Float> price;
    @FXML TableColumn<Order, Integer> amount;

    @FXML Button button;
    @FXML Button buttonSell;

    private IExchangeService exchangeService;

    public void initialize() {
        initTable();
        exchangeService = new ExchangeService(TEST);
        button.setOnAction(e -> buySample().block());
        buttonSell.setOnAction(event -> sellSample().block());
    }

    private Mono<SellResult> sellSample() {
        Player dziewan = new Player("Dziewan", apiKey, apiSecret);

        MultiValueMap<String,String > params = SellArguments.builder()
                .instrument("BTC-PERPETUAL")
                .quantity(3)
                .type("market")
                .build()
                .asParams();

        String signature = dziewan.signature(params, SELL);

        ObservableList<Order> orders = FXCollections.observableArrayList();
        long start = System.currentTimeMillis();

        return exchangeService.sell(params, signature)
                .map(resp -> resp.returnResultAs(SellResult.class))
                .map(result -> {
                    orders.addAll(result.getOrder());
                    table.setItems(orders);
                    table.refresh();
                    return result;
                });
    }

    private Mono<BuyResult> buySample() {
        Player dziewan = new Player("Dziewan", apiKey, apiSecret);

        MultiValueMap<String, String> params = BuyArguments.builder()
                .instrument("BTC-PERPETUAL")
                .quantity(3)
                .type("market")
                .build()
                .asParams();

        String signature = dziewan.signature(params, BUY);

        ObservableList<Order> orders = FXCollections.observableArrayList();
        long start = System.currentTimeMillis();

        return exchangeService.buy(params, signature)
                .map(resp -> resp.returnResultAs(BuyResult.class))
                .map(result -> {
                    orders.addAll(result.getOrder());
                    table.setItems(orders);
                    table.refresh();
                    return result;
                });
    }

    private void initTable() {
        type.setCellValueFactory(new PropertyValueFactory<>("type"));
        instrument.setCellValueFactory(new PropertyValueFactory<>("instrument"));
        price.setCellValueFactory(new PropertyValueFactory<>("price"));
        amount.setCellValueFactory(new PropertyValueFactory<>("amount"));
    }
}
